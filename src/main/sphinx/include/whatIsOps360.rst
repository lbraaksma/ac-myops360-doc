##################
What is |project|?
##################

|project| belongs to the new collection of Asset Control Services such as Data Cleansing Service, Data Browsing and the Core UI Service.
They are developed based on the MicroService architecture.

|project| is a data visualization application that shows the status of daily management processes
in a single view. |project| monitors the various data processes for data acquisition, enrichment, cleansing and delivery
and aims to make the daily operations of the AC system more efficient and transparent.

To help safeguard that data is delivered on-time and is of the expected quality, |project| monitors the following stages in the data flow process:


* Data acquisition from various data sources and loading into the AC System.

* Data enrichment from the source data, for example through normalization and consolidation.

* Data validation and cleansing to ensure that data is complete and correct.

* Data distribution to downstream systems.

It is also possible to monitor custom scripts with the |project|. A REST API is included so scripts can  make themselves "visible" to the |project|.

#################
How does it work?
#################

**************
Main Dashboard
**************

After log in, the main dashboard of |project| is shown.

.. figure:: /img/op360-dashboard-main1.png
   :alt: OPS360: Main Page
   :align: center

   |project|: Main Page

It consists of widgets for the following steps in the data flow process:

* Data Acquisition

* Data Enrichment

* Data Distribution

* Data Cleansing (Workflow)

The widgets provide summary information. From these widgets more detailed information can be requested by navigating to *Overview* and *Detailed* pages.
The sections below provide more information for each widget and navigation options.

On the top right you can select a time frame for which to show data. For example, if a 4-hour time frame is selected, the widgets will show data for the last 4 hours.


***********
Acquisition
***********

The Data Acquisition widget shows the acquisition processes that have been executed or are running within the selected time frame.
Acquisition includes processes that load new data into the AC system. It includes all interfaces that are based on *Interface Engine 4* including the *Interface Engine Configurable Loader*.
Note that the *Interface Engine* has an option for *direct normalization*. If this option is enabled, the creation and updates of the normalized data will be logged as part of the acquisition process (and not enrichment).

The widget shows no more than 10 acquisition processes.
From the widget you can click the Overview button which will open a new *Overview* page with a detailed list of acquisition processes within the selected timeframe.
If there are more than 10 processes, the "Show more" link will show. This will also bring up the *Overview* page.

From the widget and *Overview* page you can select a single process. This will open a new page with detailed information for the selected process such as:
* The files that were processed
* Any error and warning messages if they have occurred
* Related runs, eg previous runs for the same process


**********
Enrichment
**********

The Data Enrichment widget shows the enrichment processes that have been executed or are running within the selected time frame.
Enrichment includes all processes that derive new data from source data. It only includes processes that create new Normalized and Consolidated data.
For now, only normalization and consolidation of static data is shown.
Note that data loads can trigger recalculation of derived data such as normalized and consolidated data. Updates of derived data are not considered separate processes and are not visible in the dashboard.

Just as with Acquisition, the enrichment widget show no more than 10 processes.
It is possible to navigate to the derivation *Overview* page from the widget or to directly open details of one enrichment process to open the *Detailed* page for a enrichment process.

************
Distribution
************

The Data Distribution widget shows the distribution processes that have been executed or are running within the selected time frame.
Distribution includes all data deliveries made from AC Plus that have been made using the *AC Connect* product component.
All types of deliveries (Bulk, Publish/Subscribe, Request/Response) are included.

Just as with Acquisition and Enrichment, the distribution widget shows no more than 10 processes.
It is possible to navigate to the distribution *Overview* page from the widget or to directly open details of one enrichment process.

Details for a distribution process shows:

* When the distribution was executed (start and end-time)
* If the distribution completed successfully
* Any warning or errors that have occurred
* The number of ADOs and attributes that have been delivered. Note that AC Connect can include data from other ADOs (such as Issuer data for Listings). The number of ADOs also include ADOs for which related data has been delivered (if any).

*********
Cleansing
*********

Cleansing can be done via the AC Cleansing Service. This is a separate product.


***************************
Current product limitations
***************************

In the current version of |project| integration with the following AC components is *not* available:

 * Price Consolidation

 * AC Contour

 * AC Gateways

Also there is a known limitation in regard to rerunning Interface Engine jobs. With the *ierun* utility, an interface batch load can be performed for a configured *Task*.
Within each *Task* one or more *Jobs* are executed. If a *Job* failed it can be rerun. This rerun is not yet captured in |project|.

The *AC compatibility matrix* document available on the customer portal shows which versions of the various products are compatible with |project|.


############################
AC MicroService Architecture
############################

A MicroServices architecture is a collection of independently deploy-able services.
Every service has its own function and will be deployed in its own Docker container.
Every container has its own isolated workload environment making it independently deploy-able and scalable.
Each individual AC Service runs in its own isolated environment, separate from the others within the architecture.
Some of the new AC Services are Web applications having their own Web User Interfaces (UI). They are developed to
provide the the ability to configure a single unified Web UI (AC Web UI).
The AC Core UI Service is the core component of the AC Services representing the common properties of this UI.
