##############
Wrapper Script
##############

Custom Unix scripts can be integrated into |project| using a wrapper script.
Instead of executing the custom script directly, the wrapper script will be executed that takes the custom script as input parameter (with all its parameters).
The wrapper script needs to:

*	create a new process in |project| that allows to track the custom script execution;
*	execute the custom script;
*	capture warning, error and info messages from the custom script output (stderr and stdout) and send them to |project|;
*	close the process in |project| with the correct end status as soon as the custom script has finished;
*   exit with same exit code as custom script;
*   use linux tools wget or curl to call the API.
*   authenticate using a cookie, manually retrieved by logging in to the AC WebService UI;

When the script is using a URL of the RestAPI it needs to provide a cookie in the header.
This cookie you can get by manually logging in to the AC WebService UI.
You will receive a cookie you need to put in the header when requesting a URL of the Rest API.
Please take care of the expiration of the cookie.
When the expiration feature is enabled for the Process Tracking Web Service you will need to renew this cookie.

The custom script needs to:

*	Sent Warning, error and info messages stdout and stderr
*	Create messages with a specific format using the environment variables AC_WARNING_PREFIX, AC_ERROR_PREFIX, AC_INFO_PREFIX

All processes created this way will be saved as separate entities.
They will unite with processes from Interface engine and AC Connect only when feeding updates to UI.


************************
REST API: Create Process
************************

.. code-block:: console

    POST {BASE_URL}/api/v1/process
    where {BASE_URL} is the server and port number (e.g. http://ops360server:6969 )

**Description**

Adds new process that can be monitored in the UI of |project|.


.. tabularcolumns:: |X|X|X[6]|X|
.. API Create new process:
.. list-table:: REST API, Parameters; Create new process
   :header-rows: 1
   :class: small

   * - Parameters
     - Required
     - Usage
     - Example

   * - name={string}
     - yes
     - Name of the process that allows to link it with previous instances of the same process
     - price_consolidation

   * - stage={string}
     - yes
     - Stage of the process (acquisition, derivation, distribution)
     - acquisition


**Response**

•	id - ID of the process

**Error messages**

•	401 - Unauthorized(if Authentication is implemented)
•	405 - invalid input


********************
REST API Post update
********************

.. code-block:: console

    POST {BASE_URL}/api/v1/process/<processId>
    where {BASE_URL} is the server and port number (e.g. http://ops360server:6969 )

**Description**

Post new update with message and/or progress amount for a process that has been created in |project|.
This will update the process with id equals to the processId that has been created.

.. tabularcolumns:: |X[3]|X|X[6]|X[3]|
.. API Post new update:
.. list-table:: REST API, Parameters; Post new update with message and/or progress amount
   :header-rows: 1
   :class: small

   * - Parameters
     - Required
     - Usage
     - Example

   * - current_progress={number}
     - no
     - Object representing current progress.
     - 15

   * - max_progress={number}
     - no
     - Object representing maximum progress
     - 55

   * - message={string}
     - no
     - Text of the message.
     - Error in file 1

   * - level={enum}
     - no
     - Text of the message.
     - Can be: info, warning, error	warning

   * - metrics={map}
     - no
     - Collection of key-value pairs representing arbitrary metrics that client wants to write for some steps of the process.
     - { "ados":34, "attributes":15, "files":5 }

**Error messages**

•	401 - Unauthorized(if Authentication is implemented)
•	405 - invalid input


******************************
REST API Post process finished
******************************

.. code-block:: console

    POST {BASE_URL}/api/v1/process/<processId>
    where {BASE_URL} is the server and port number (e.g. http://ops360server:6969 )

**Description**

Marks a process to be finished  with a result message for process in |project| with id equal to processID.


.. tabularcolumns:: |X[2]|X|X[6]|X[4]|
.. API Terminal message:
.. list-table:: REST API, Parameters; Post terminal message for process
   :header-rows: 1
   :class: small

   * - Parameters
     - Required
     - Usage
     - Example

   * - status={enum}
     - yes
     - Field indicating terminal status for process.
     - Can be: successful, failed

   * - message={string}
     - no
     - Optional message to print an error in case of a failure or some additional info in case of success.
     - ERROR in line 156

**response**


**Error messages**

•	401 - Unauthorized(if Authentication is implemented)
•	405 - invalid input