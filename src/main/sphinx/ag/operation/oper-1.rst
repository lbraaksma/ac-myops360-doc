****************************
Starting the |project|
****************************

Start the container using the docker-compose command from the folder where your docker-compose file resides:

.. code-block:: console

   docker-compose up -d


****************************
Stopping the |project|
****************************

Stop the container using the docker-compose command from the folder where your docker-compose file resides:

.. code-block:: console

   docker-compose stop

This will stop the container.