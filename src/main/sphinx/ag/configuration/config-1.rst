The |project| can be configured with the following steps:

* **Step 1.** Add properties
* **Step 2.** Configure AC Connect and add properties
* **Step 3.** Provide Service permissions to users
* **Step 4.** Make the Service available to a proxy service


Public API to support custom scripts (stable enough?)

**Step 1.**

The following properties can be added to the *override.properties* file.
In our docker-compose example we have put this file on volume ``/myhost/docker-volumes/ops-server/config``.
And we added environment variable ``"SPRING_CONFIG_LOCATION=classpath:/config, file:///override/override.properties"`` to the docker-compose file so the the container can find this file.

All of the mentioned properties below are used by the *ops-server* container, the *process-tracking* container only uses the authorization properties.

Some of these |project| properties can also be provided as environment variables.

.. important::

   Changes to configuration take affect after |project| restart.


*AC Server properties*

Properties that are used for connecting to AC Server

.. acprop:: ac.api.host

    Host where AC Server is running

.. acprop:: ac.api.installation

    Name of AC Server installation (as specified in ac.ini file)

.. acprop:: ac.api.user

    User who is used to connect to AC Server (acdba by default)

.. acprop:: ac.api.password

    Password of ``ac.api.user``

.. acprop:: ac.api.passwordFileName

    Name of the file that contains password for ``ac.api.user``

    .. important::

    Only one property should be used (either ``ac.api.password`` or ``ac.api.passwordFileName``)

.. acprop:: ac.api.queriesMax

    Maximum number of query server threads used

.. acprop:: ac.api.updatesMax

    Maximum number of update server threads used


*Database properties*


|project| stores its own data and requires an Oracle tablespace.

.. acprop:: spring.datasource.url

    url to access database, for example *jdbc:oracle:thin:@172.24.130.50:1521:ORCL*

.. acprop:: spring.datasource.username

    user to connect to database. This is a decidacted Oracle user which has been created during initial installation of |project|.

.. acprop:: spring.datasource.password

    password for user specified in spring.datasource.username

.. acprop:: spring.datasource.driver-class-name

     for example oracle.jdbc.driver.OracleDriver


*Interface Engine properties*

|project| can receive messages from Interface Engine, track and visualize progress of all **ierun** runs.

.. acprop:: ops.interface-engine.task-timeout-seconds

    How long |project| waits before considering Interface Engine task as having UNKNOWN status (in seconds).

.. acprop:: ops.scheduler.interface-engine-messages-period-ms

    frequency in milliseconds at which |project| processes messages received from Interface Engine


*Common properties*


.. acprop:: server.port

    Port number at which application accepts API calls from UI. By default it is set to **8080**

.. acprop:: server.servlet.session.timeout

    Number of seconds after which the user session will time out. When the user session has timed out the user will need to login again. Time out will only occur if there was no activity from the user.


*Authorization properties*


.. acprop:: ops.auth.keystore-file

    Path to a keystore file. Keystore file should contain a public key paired with private key used by AC Authentication

.. acprop:: ops.auth.keystore-password

    Password for a keystore file

.. acprop:: ops.auth.key-alias

    Alias of a public key contained in store.

.. acprop:: ops.auth.algorithm

    Algorithm that is used by AC Authentication

**Step 2.**

|project| can track Subscriptions managed by AC Connect and display relevant statistics.

If AC Connect is installed:
Ensure the AC Connect *config.service* plugin is enabled. Locate the file ``plugin.properties`` in the AC Connect subdirectory ``config/plugins/config.service`` and set the property config.service.enabled to true.

.. code-block:: console

          config.service.enabled=true
          # HTTP address where the service is available for RESTful requests
          config.service.address=http://0.0.0.0:8989

Ensure that AC Connect logs subscription runs. This can be enabled by setting property ``dsi.subslogging.enable`` to true in ``ac.properties`` located in the AC Connect *config* directory.
If AC Connect is not installed, disable AC Connect integration by setting property ``ops.ac-connect.enabled`` to false in the *override.properties* file of |project|.

.. acprop:: ops.ac-connect.enabled

    property to enable integration monitoring of distribution with AC Connect (default true)

Other AC Connect properties that can be overridden in the file *override.properties*


.. acprop:: ops.ac-connect.scheduler-period-ms

    frequency in milliseconds at which |project| retrieves distribution information from AC Connect (default 10000)

.. acprop:: ops.ac-connect.connections[0].host

    location where |project| can retrieve information from AC Connect. This needs to point to the IP address and port number of the AC Connect *config.service*.


    For Example:  ``ops.ac-connect.connections[0].host=127.0.0.1:8989``

    Multiple AC Connect instances can be included. For example, to include a second instance of AC Connect, specify the property ``ops.ac-connect.connections[1].host``.

.. acprop:: ops.ac-connect.cache-expire-time-seconds

    controls amount of time for which |project| keeps retrieved Subscription data in internal cache to make UI more responsive and make fewer calls to AC Connect



**Step 3.**


Users need to be given access to |project|. The user needs to have a *role* assigned that gives permission to access |project|.
Role permissions are defined in the **AC Authentication** as services.
For a selected role one of the following services can be added:

.. acprop:: OPS.ACQUISITION

     Access to Acquisition in Ops360

.. acprop:: OPS.ENRICHMENT

     Access to Enrichment in Ops360

.. acprop:: OPS.DISTRIBUTION

     Access to Distribution in Ops360

.. acprop:: OPS.CLEANSING

     Access to Cleansing in Ops360


Do not forget to add these permissions to the file ``services.json`` of the AC Authentication Service.
See also:
**AC Authentication** Administration guide


**Step 4.**

Make the Service available to a proxy service by specifying:

.. code-block:: console

   proxy all /ops360/subscription to ops-server:8080/ops360/subscription
   proxy all /ops360 to ops-server:8080/ops360

As an example, when a NGINX server is used as a proxy server, the configuration file needs to be updated.
The configuration file of a NGINX server can be found in the folder ``/etc/nginx/available-sites``.
The following lines need to be added to this file:

.. code-block:: console

   location /ops360/subscription {
     proxy_pass http://ops-server:8080/ops360/subscription;
     proxy_http_version 1.1;
     proxy_set_header Upgrade $http_upgrade;
     proxy_set_header Connection "Upgrade";
     access_log  /var/log/nginx/access.log  upstream_logging;
   }


   location /ops360 {
     proxy_pass http://ops-server:8080/ops360;
     access_log  /var/log/nginx/access.log  upstream_logging;
   }
