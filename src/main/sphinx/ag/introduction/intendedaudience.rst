*****************
Intended Audience
*****************

This guide is for anyone responsible for setting up and using |project|.

Your role may include one or more of the following:

* System Administrator
   Installation, initial configuration, starting and stopping the software.

* Data Administrator, Business Analyst
   Configuring business data models.


******************
Additional Reading
******************

.. seealso::

   * AC Core UI Administration Guide.
   * AC Authentication Service
   * AC Server Technical Reference Guide.

