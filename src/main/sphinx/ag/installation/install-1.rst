*****************************
Minimal Software Requirements
*****************************

* Docker
* Proxy service (e.g. a separate NGNIX container)
* AC Core UI Service
* AC Authentication Service
* AC Server 6.3 (or higher)
* Oracle tablespace. The existing tablespace from *AC Server* can be used but for production it is adviced to use a separate table space.
* Latest AC interfaces that are compatible to send updates to |project|, check Release Nodes.
* Latest AC Connect that is compatible to send updates to |project|, check Release Nodes.
* AC Application Data A0.

*AC Application Data A0* installation steps are described in the 'AC Application Data A0 Administration Guide'.
The product contains the following static attributes that |project| requires:

- A0_OPS-NODE ID
- A0_OPS CONFIG

All software requirements must be met prior to installing the |project| . If not, please resolve this first.


******************************
Installing the |project|
******************************

The |project| is provided as a package with two Docker image files and some scripts which you can download from the Asset Control Userweb as follows:

   - Log onto the Asset Control `Userweb <https://services.asset-control.com/userweb>`_. Login
     credentials are required to access this site.
   - In the “AC Plus Applications” section, follow the link for “AC OPS360 DASHBOARD".
   - Click the link in the "Application builds" section to download the package file.

Copy all the files except for the image files to the AC Plus Server.

Create database user 'ops' in AC Plus database:
Update password for user 'ops' in file ``create_user_oracle.sql``.
This file will be delivered in the package.
Create user by running the command on the AC Plus server:

.. code-block:: console

         sqlplus / as sysdba @create_user_oracle.sql

Another user name can be used as well as long as the correct user name is overridden in the *override.properties* file (will be described in chapter `configuration`).
The user needs to have certain permissions. The file ``create_user_oracle.sql`` lists the permissions that need to be granted.


By default |project| uses port number 8090 (see docker-compose file). If this port is already in use it can be changed in the *override.properties* file (will be described in chapter `configuration`).
If the port number is changed, also the port number included in the file ``$AC_SYSTEM/operations/scripts/init_config_ado.acbl`` must get updated to the new port number.
This "acbl" script is loaded with the wrapper script ``init_configuration.sh`` which is in the package.

Load the acbl by running the wrapper script on the AC Plus Server with the following command:

.. code-block:: console

          ksh ./init_configuration.sh

Edit on the AC Plus Server ``liquibase.properties`` file by providing the AC Plus database details (see example below):

.. code-block:: console

        url=jdbc:oracle:thin:@ACPlusServer:1521:ORACLESID
        username=ops
        password=password
        driver=oracle.jdbc.driver.OracleDriver

Migrate database by running on the AC Plus server:

.. code-block:: console

      ksh ./migrate.sh


.. important::

        Database migration has to be performed after each upgrade to newer version of |project|.



The docker image files for |project| are also contained in the `*.tar.gz` archive.
Unzip the package to load the Docker image files to your local Docker repository.
For example let's say the image name is ops360dashboard-service:1.0.0 you can use the command:


.. code-block:: console

   docker load -i <ops360dashboard-service-1.0.0-imagefile>.tar.gz

Load both image files (one for the Process-Tracking and one for the Ops-Server) to your Docker repository.

In this Administration Guide we will use the docker-compose utility to start the containers.

The Process Tracking container will start a database in the folder ``/DB`` inside the container.
In order to preserve this database when the container is stopped it is recommended to bind mount a directory (volume) on the host machine to the this folder inside the container.
In our docker-compose example a volume ``/myhost/docker-volumes/ops-server/process-tracking-DB`` has been created and mounted to the folder ``/DB`` of the |project| container.
So the database is created in this volume.

With the ``volumes`` parameter in a docker-compose file you can do this mount.
A docker-compose file,  :file:`docker-compose.yml` would look like:

.. code-block:: console

   process-tracking:
     image: "localdockerrepo/process-tracking:1.0.0"
     container_name: process-tracking
     hostname: process-tracking
     expose:
       - "6565"
     ports:
       - "8090:8080"
     environment:
       - "spring.datasource.url=jdbc:h2:file:/DB/main;DB_CLOSE_ON_EXIT=FALSE;INIT=CREATE SCHEMA IF NOT EXISTS AC\\;SET SCHEMA AC"
       - "ops.auth.enabled=false"
       - "SPRING_CONFIG_LOCATION=classpath:/config, file:///override/override.properties"
     volumes:
       - "/myhost/docker-volumes/ops-server/process-tracking-DB:/DB:Z"
       - "/myhost/docker-volumes/ops-server/config:/override:Z"
     networks:
       - ac-net


   ops-server:
     image: "localdockerrepo/server:1.0.0"
     container_name: ops-server
     hostname: ops-server
     expose:
       - "8080"
     ports:
       - "8080:8080"
     volumes:
       - "/myhost/docker-volumes/ops-server/config:/override:Z"
     environment:
       - "SPRING_CONFIG_LOCATION=classpath:/config, file:///override/override.properties"
       - "ac.api.host=ops360server.company.com"
       - "spring.datasource.url=jdbc:oracle:thin:@ops360server.company.com:1521:ORCL"
       - "ops.ac-connect.connections[0].host=ops360server.company.com:8989"
       - "tz=Europe/Amsterdam"
     networks:
       - ac-net

   networks:
     ac-net:
     external: true


Both containers have the ability to override properties with a file ``override.properties``.
This file resides inside the containers in the folder ``/override``.
This folder in both containers has been bind mounted to the volume ``/myhost/docker-volumes/ops-server/config`` so it can be shared.
The properties that can be overridden are described in the "Configuration" chapter.
Some of these properties have been used here as environment variables.

In this example containers are configured to use a virtual docker `ac-net` network.
Port 8080 will be exposed to access the |project| outside the `ac-net` network.

********************************
Upgrading the |project|
********************************

Stop and remove the container (when using docker-compose) but do not remove the volume.
Run the following command from the folder where your docker-compose file resides:

.. code-block:: console

   docker-compose down


Download the new image from `Userweb <https://services.asset-control.com/userweb>`.
Load the new image to your local Docker repository.
Update the docker-compose file with the new image.


********************************
Uninstall the |project| Service
********************************

To stop and remove the container and remove volume (when using docker-compose).
Run the following command from the folder where your docker-compose file resides:


.. code-block:: console

   docker-compose down --volumes


It is not common practice to remove the image as the old image can be kept without problem.
Look for the `image id`  with command:

.. code-block:: console

   docker images

However if you wish to remove the image the command is:


.. code-block:: console

   rmi imageid

If you have made changes to the configuration file of a proxy server for the |project| do not forget to delete them as well.
